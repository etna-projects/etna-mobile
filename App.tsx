import { useFonts } from 'expo-font';
import { StatusBar } from 'expo-status-bar';
import {
  TamaguiProvider,
  Theme,
} from 'tamagui';
import React from 'react';
import { SafeAreaProvider, initialWindowMetrics } from 'react-native-safe-area-context';
import { NavigationContainer } from '@react-navigation/native';
import { useFlipper } from '@react-navigation/devtools';
import { ToastProvider, ToastViewport } from '@tamagui/toast';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { ActivityIndicator } from 'react-native';
import config from './tamagui.config';
import MainNavigator from '@/navigation';
import { navigationRef } from '@/navigation/utils';
import { persistor, store } from '@/redux';
import extendDayjs from '@/utils/dayjs';

extendDayjs();
export default function App() {
  useFlipper(navigationRef);

  const [loaded] = useFonts({
    Inter: require('@tamagui/font-inter/otf/Inter-Medium.otf'),
    InterBold: require('@tamagui/font-inter/otf/Inter-Bold.otf'),
  });

  if (!loaded) {
    return null;
  }

  return (
    <Provider store={store}>
      <PersistGate
        loading={<ActivityIndicator size="large" />}
        persistor={persistor}
      >
        <TamaguiProvider config={config}>
          <Theme name="light">
            <SafeAreaProvider initialMetrics={initialWindowMetrics}>
              <ToastProvider>
                <NavigationContainer
                  ref={navigationRef}
                >
                  <MainNavigator />
                </NavigationContainer>
                <ToastViewport />
              </ToastProvider>
              <StatusBar style="dark" animated />
            </SafeAreaProvider>
          </Theme>
        </TamaguiProvider>
      </PersistGate>
    </Provider>
  );
}
