/* eslint-disable no-param-reassign */
import { MigrationManifest, PersistedState } from 'redux-persist';
import { MigrationConfig } from 'redux-persist/es/createMigrate';
import { RootState } from './index';

const manifest: MigrationManifest = {
  0: (state: PersistedState & RootState) => state,
};

const options: MigrationConfig = {
  debug: __DEV__,
};

export { manifest, options };
