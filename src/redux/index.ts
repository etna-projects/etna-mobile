import { configureStore, Middleware, combineReducers } from '@reduxjs/toolkit';
import { TypedUseSelectorHook, useSelector, useDispatch } from 'react-redux';
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
  PersistConfig,
  createMigrate,
} from 'redux-persist';
import { reduxStorage } from '@/utils/mmkv';
import { manifest, options } from './migrate';
import { userReducer, userReducerName } from '@/redux/reducers/user/user.slice';
import { appReducer, appReducerName } from '@/redux/reducers/app/app.slice';
import { notificationsReducer, notificationsReducerName } from '@/redux/reducers/notifications/notifications.slice';

const persistConfig: PersistConfig<RootState> = {
  key: '@etna',
  storage: reduxStorage,
  whitelist: [
    userReducerName,
    notificationsReducerName,
  ],
  blacklist: [],
  version: 1,
  migrate: createMigrate(manifest, options),
};

const middlewares: Middleware[] = [];

if (__DEV__) {
  const createDebugger = require('redux-flipper').default;
  middlewares.push(createDebugger());
}

const rootReducer = combineReducers({
  [userReducerName]: userReducer,
  [appReducerName]: appReducer,
  [notificationsReducerName]: notificationsReducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    serializableCheck: __DEV__ ? false : {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
    },
    immutableCheck: false,
  }).concat(...middlewares),
});

const persistor = persistStore(store);

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof rootReducer>;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export { store, persistor };
