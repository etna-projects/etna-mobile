import { createAsyncThunk } from '@reduxjs/toolkit';
import thunkErrorHandler from '@/utils/thunkErrorHandler';
import intraApi from '@/api/IntraApi';
import { RootState } from '@/redux';

export const getAllNotifications = createAsyncThunk(
  'notifications/getAllNotifications',
  async (_, { getState }) => {
    try {
      const state = getState() as RootState;
      const { data } = await intraApi.getArchivedNotifications(state.user.login);
      return data;
    } catch (error) {
      return thunkErrorHandler(error);
    }
  },
);

export const markNotificationAsRead = createAsyncThunk(
  'notifications/markNotificationAsRead',
  async (notificationId: number, { getState }) => {
    try {
      const state = getState() as RootState;
      const { data } = await intraApi.markNotificationAsRead(state.user.login, notificationId);
      return data;
    } catch (error) {
      return thunkErrorHandler(error);
    }
  },
);
