import { createSlice } from '@reduxjs/toolkit';
import { getAllNotifications, markNotificationAsRead } from '@/redux/reducers/notifications/notifications.thunks';

interface NotificationState {
  list: UserNotification[];
}

const initialState: NotificationState = {
  list: [],
};

const notificationsSlice = createSlice({
  name: 'notifications',
  initialState,
  reducers: {
    reset: (state) => {
      Object.assign(state, initialState);
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getAllNotifications.fulfilled, (state, { payload }) => {
        state.list = payload;
      })
      .addCase(markNotificationAsRead.fulfilled, (state, { payload }) => {
        const index = state.list.findIndex((notification) => notification.id === payload.id);
        state.list[index] = payload;
      });
  },
});

export const {
  name: notificationsReducerName,
  reducer: notificationsReducer,
  actions: notificationsActions,
} = notificationsSlice;
