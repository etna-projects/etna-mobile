import { createSlice } from '@reduxjs/toolkit';
import { getCurrentUserProfile } from '@/redux/reducers/user/user.thunks';

interface UserState {
  id: number;
  login: string;
  email: string;
  firstname: string;
  lastname: string;
  profile_picture: string;
  close: boolean;
  roles: string[];
  created_at: Date | null;
  updated_at: Date | null;
  deleted_at: Date | null;
  promo: string;
  promotions: UserPromotion[];
}

const initialState: UserState = {
  id: 0,
  login: '',
  email: '',
  firstname: '',
  lastname: '',
  profile_picture: '',
  close: false,
  roles: [],
  created_at: null,
  updated_at: null,
  deleted_at: null,
  promo: '',
  promotions: [],
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    reset: (state) => {
      Object.assign(state, initialState);
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getCurrentUserProfile.fulfilled, (state, action) => {
        state.id = action.payload.identity.id;
        state.login = action.payload.identity.login;
        state.email = action.payload.identity.email;
        state.firstname = action.payload.profile.firstname;
        state.lastname = action.payload.profile.lastname;
        state.close = action.payload.profile.close;
        state.roles = action.payload.profile.roles;
        state.created_at = action.payload.profile.created_at;
        state.updated_at = action.payload.profile.updated_at;
        state.deleted_at = action.payload.profile.deleted_at;
        state.promotions = action.payload.promo;
        state.promo = action.payload.profileBrief.promo;
        state.profile_picture = action.payload.profilePicture;
      });
  },
});

export const {
  reducer: userReducer,
  name: userReducerName,
  actions: userActions,
} = userSlice;
