import { createAsyncThunk } from '@reduxjs/toolkit';
import thunkErrorHandler from '@/utils/thunkErrorHandler';
import authApi from '@/api/Auth';
import prepIntraApi from '@/api/PrepIntra';
import intraApi from '@/api/IntraApi';

export const authenticate = createAsyncThunk(
  'user/authenticate',
  async (payload: { login: string; password: string }) => {
    try {
      await authApi.login(payload.login, payload.password);
    } catch (e) {
      thunkErrorHandler(e);
    }
  },
);

export const getIdentity = createAsyncThunk(
  'user/getIdentity',
  async () => {
    try {
      const { data } = await authApi.identity();
      return data;
    } catch (e) {
      return thunkErrorHandler(e);
    }
  },
);

export const getProfileById = createAsyncThunk(
  'user/getProfileById',
  async (id: number) => {
    try {
      const { data } = await authApi.userById(id);
      return data;
    } catch (e) {
      return thunkErrorHandler(e);
    }
  },
);

export const getProfileBriefByLogin = createAsyncThunk(
  'user/getProfileBriefByLogin',
  async (login: string) => {
    try {
      const { data } = await intraApi.getUserByLogin(login);
      return data;
    } catch (e) {
      return thunkErrorHandler(e);
    }
  },
);

export const getCurrentUserPromotion = createAsyncThunk(
  'user/getCurrentUserPromotion',
  async () => {
    try {
      const { data } = await prepIntraApi.getPromotion();
      return data;
    } catch (e) {
      return thunkErrorHandler(e);
    }
  },
);

export const getCurrentUserProfile = createAsyncThunk(
  'user/getCurrentUserProfile',
  async (_, { dispatch }) => {
    try {
      const data = await dispatch(getIdentity()).unwrap();
      const profile = await dispatch(getProfileById(data.id)).unwrap();
      const profileBrief = await dispatch(getProfileBriefByLogin(data.login)).unwrap();
      if (profile.close) throw new Error('Votre compte est close, veuillez contacter l\'administration ETNA.');
      const promo = await dispatch(getCurrentUserPromotion()).unwrap();
      const profilePicture = authApi.getProfilePictureUrl(data.login);
      return {
        identity: data,
        profile,
        promo,
        profilePicture,
        profileBrief,
      };
    } catch (e) {
      return thunkErrorHandler(e);
    }
  },
);
