import { createSlice } from '@reduxjs/toolkit';
import { getNetworkStatus } from '@/redux/reducers/app/app.thunks';

interface AppState {
  isInternetReachable: boolean;
}

const initialState: AppState = {
  isInternetReachable: true,
};

const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    reset: (state) => {
      Object.assign(state, initialState);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getNetworkStatus.fulfilled, (state, action) => {
      state.isInternetReachable = action.payload.isInternetReachable || false;
    });
  },
});

export const {
  name: appReducerName,
  reducer: appReducer,
  actions: appActions,
} = appSlice;
