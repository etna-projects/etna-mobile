import { createAsyncThunk } from '@reduxjs/toolkit';
import * as Network from 'expo-network';
import thunkErrorHandler from '@/utils/thunkErrorHandler';
import { getCurrentUserProfile, getIdentity } from '@/redux/reducers/user/user.thunks';
import { userActions } from '@/redux/reducers/user/user.slice';
import { notificationsActions } from '@/redux/reducers/notifications/notifications.slice';

export const getNetworkStatus = createAsyncThunk(
  'network/getNetworkStatus',
  async () => {
    try {
      return await Network.getNetworkStateAsync();
    } catch (error) {
      throw new Error('Impossible de récupérer le status du réseau');
    }
  },
);

export const resetAppState = createAsyncThunk(
  'reset/resetAppState',
  async (_, { dispatch }) => {
    dispatch(userActions.reset());
    dispatch(notificationsActions.reset());
  },
);

export const handleStartupTasks = createAsyncThunk(
  'startup/handleStartupTasks',
  async (_, { dispatch }) => {
    try {
      try {
        await dispatch(getIdentity()).unwrap();
      } catch (error) {
        await dispatch(resetAppState()).unwrap();
        return { authenticated: false };
      }
      await dispatch(getCurrentUserProfile()).unwrap();
      return { authenticated: true };
    } catch (error) {
      return thunkErrorHandler(error);
    }
  },
);
