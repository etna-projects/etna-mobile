import * as yup from 'yup';

export const LoginSchema = yup.object().shape({
  login: yup.string().required('Requis'),
  password: yup.string().required('Requis'),
});
