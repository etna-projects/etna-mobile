import dayjs from 'dayjs';
import LocalizedFormat from 'dayjs/plugin/localizedFormat';
import RelativeTime from 'dayjs/plugin/relativeTime';
import 'dayjs/locale/fr';

const extendDayjs = () => {
  dayjs.extend(LocalizedFormat);
  dayjs.extend(RelativeTime);
  dayjs.locale('fr');
};

export default extendDayjs;
