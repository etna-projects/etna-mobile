import axios, { AxiosError } from 'axios';

/**
 * Handles errors thrown by thunks, and throws an error if the error is not an axios error.
 * @param e {unknown} - the thrown error
 * @param code {boolean} - if true, the status code will be thrown instead of the error message
 * @returns {never} - it just throws an error :)
 */

const thunkErrorHandler = (e: unknown, code = false) => {
  if (__DEV__) console.debug('thunkErrorHandler', e);
  const isAxiosError = axios.isAxiosError(e);
  if (!isAxiosError) throw e;
  const error = e as AxiosError | undefined;
  if (e?.response?.status && code) {
    const statusCode = error?.response?.status;
    throw new Error(statusCode?.toString());
  }
  if (error?.response?.data) {
    const data = error.response.data as any | undefined;
    throw new Error(data?.message ?? 'Une erreur s\'est produite');
  }
  throw error ?? new Error('Une erreur s\'est produite');
};

export default thunkErrorHandler;
