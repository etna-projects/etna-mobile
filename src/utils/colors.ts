export const ETNA_BLUE = '#337ab7';
export const ETNA_BLUE_BG = '#d9edf7';

export const ETNA_GREEN = '#4B94A5';
export const ETNA_GREEN_SUCCESS = '#5cb85c';
export const ETNA_GRAY = '#d9edf7';
export const ETNA_RED = '#d9534f';
