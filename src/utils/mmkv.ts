import { MMKV } from 'react-native-mmkv';
import { Storage } from 'redux-persist';
import { initializeMMKVFlipper } from "react-native-mmkv-flipper-plugin";

const mmkv = new MMKV();
if (__DEV__) {
  initializeMMKVFlipper({ default: mmkv });
}
export const reduxStorage: Storage = {
  setItem: (key, value) => {
    mmkv.set(key, value);
    return Promise.resolve(true);
  },
  getItem: (key) => {
    const value = mmkv.getString(key);
    return Promise.resolve(value);
  },
  removeItem: (key) => {
    mmkv.delete(key);
    return Promise.resolve();
  },
};

export default mmkv;
