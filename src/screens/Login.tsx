import { useFormik } from 'formik';
import { Asserts } from 'yup';
import {
  Button,
  Form,
  Input,
  Label,
  Paragraph,
  Spinner,
  Image,
  YStack, H4, XStack,
} from 'tamagui';
import {
  Eye as IconEye,
  EyeOff as IconEyeOff,
} from '@tamagui/lucide-icons';
import { useState } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useToastController } from '@tamagui/toast';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { LoginSchema } from '@/utils/validators/auth.yup';
import EtnaLogo from '@/assets/images/logo/etna-logo-1-quadri.png';
import { useAppDispatch } from '@/redux';
import { authenticate, getCurrentUserProfile } from '@/redux/reducers/user/user.thunks';
import { ETNA_BLUE } from '@/utils/colors';

type FormValues = Asserts<typeof LoginSchema>;
export default function Login() {
  const dispatch = useAppDispatch();
  const [hidePassword, setHidePassword] = useState(true);
  const navigation = useNavigation();
  const toast = useToastController();
  const form = useFormik<FormValues>({
    validationSchema: LoginSchema,
    initialValues: {
      login: '',
      password: '',
    },
    onSubmit: async (values) => {
      try {
        await dispatch(authenticate(values)).unwrap();
        await dispatch(getCurrentUserProfile()).unwrap();
        toast.show(
          'Connexion réussie',
          {
            native: true,
            duration: 1500,
            message: 'Vous êtes connecté !',
            burntOptions: {
              preset: 'done',
              haptic: 'success',
            },
          },
        );
        navigation.navigate('home');
      } catch (error: any) {
        toast.show(
          'Une erreur est survenue',
          {
            native: true,
            duration: 3000,
            message: error.message,
            burntOptions: {
              preset: 'error',
              haptic: 'error',
            },
          },
        );
      }
    },
  });

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Form
        flex={1}
        onSubmit={form.handleSubmit}
        alignItems="center"
        justifyContent="center"
      >
        <YStack padding="$3" minWidth={350} space="$4" paddingTop="$2">
          <View style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}
          >
            <Image
              source={EtnaLogo}
              resizeMode="contain"
              width={200}
              height={200}
            />
          </View>
          <H4 textAlign="center">Connectez vous avec vos identifiants intra</H4>
          <>
            <Label htmlFor="login">Login</Label>
            <Input
              id="login"
              onChangeText={(text) => form.setFieldValue('login', text)}
              onBlur={form.handleBlur('login')}
              value={form.values.login}
              autoCapitalize="none"
              autoComplete="username"
            />
            {form.touched.login && form.errors.login && (
              <Paragraph color="red">{form.errors.login}</Paragraph>
            )}
          </>
          <>
            <Label htmlFor="password">Mot de passe</Label>
            <XStack>
              <Input
                flex={1}
                id="password"
                onChangeText={(text) => form.setFieldValue('password', text)}
                onBlur={form.handleBlur('password')}
                value={form.values.password}
                autoCapitalize="none"
                autoComplete="password"
                secureTextEntry={hidePassword}
              />
              <Button
                icon={hidePassword ? <IconEye size="$1" /> : <IconEyeOff size="$1" />}
                onPress={() => setHidePassword((p) => !p)}
              />
            </XStack>
            {form.touched.password && form.errors.password && (
            <Paragraph color="red">{form.errors.password}</Paragraph>
            )}
          </>
          <View style={{ flex: 1 }} />
          <Form.Trigger asChild disabled={!form.isValid}>
            <Button
              backgroundColor={ETNA_BLUE}
              color="white"
              icon={form.isSubmitting ? <Spinner /> : undefined}
            >
              Se connecter
            </Button>
          </Form.Trigger>
        </YStack>
      </Form>
    </SafeAreaView>
  );
}
