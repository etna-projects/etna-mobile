import {
  H4, XStack, Text, Image, YStack,
} from 'tamagui';
import { SafeAreaView } from 'react-native-safe-area-context';
import { StatusBar } from 'expo-status-bar';
import { useNavigation } from '@react-navigation/native';
import { Pressable, View } from 'react-native';
import {
  Mail as IconMail,
  Edit as IconEdit,
  BarChart3 as IconBarChart3,
  Laptop as IconLaptop,
} from '@tamagui/lucide-icons';
import dayjs from 'dayjs';
import { ETNA_GREEN, ETNA_GREEN_SUCCESS } from '@/utils/colors';
import { useAppSelector } from '@/redux';

export default function Menu() {
  const navigation = useNavigation();
  const profilePictureUrl = useAppSelector((state) => state.user.profile_picture);
  const firstname = useAppSelector((state) => state.user.firstname);
  const lastname = useAppSelector((state) => state.user.lastname);
  const email = useAppSelector((state) => state.user.email);
  const promo = useAppSelector((state) => state.user.promo);
  return (
    <SafeAreaView
      style={{
        backgroundColor: ETNA_GREEN,
        flex: 1,
      }}
    >
      <StatusBar style="light" />
      <Pressable
        onPress={() => navigation.navigate('profile')}
      >
        <XStack space marginHorizontal="$4">
          <Image
            source={{
              uri: profilePictureUrl,
              height: 240,
              width: 200,
            }}
            height={84}
            width={70}
            resizeMode="contain"
            borderRadius="$2"
          />
          <YStack>
            <H4 color="white">
              {`${firstname} ${lastname}`}
            </H4>
            <Text color="white">
              {email}
            </Text>
            <XStack
              backgroundColor={ETNA_GREEN_SUCCESS}
              borderRadius="$2"
              padding="$1"
              paddingHorizontal="$2"
              alignItems="center"
              space="$1"
              marginTop="$3"
            >
              <Text
                color="white"
                fontWeight="bold"
                fontSize="$2"
              >
                {`Promo ${promo}`}
              </Text>
            </XStack>
          </YStack>
        </XStack>
      </Pressable>
      <View style={{ flex: 1 }} />
      <YStack space marginHorizontal="$4">
        <Pressable
          onPress={() => navigation.navigate('tickets')}
          style={{
            justifyContent: 'center',
          }}
        >
          <XStack alignItems="center" space>
            <IconMail color="white" />
            <H4 color="white">
              Tickets
            </H4>
          </XStack>
        </Pressable>
        <Pressable
          onPress={() => navigation.navigate('grades')}
          style={{
            justifyContent: 'center',
          }}
        >
          <XStack alignItems="center" space>
            <IconBarChart3 color="white" />
            <H4 color="white">
              Notes
            </H4>
          </XStack>
        </Pressable>
        <Pressable
          onPress={() => navigation.navigate('declarations')}
          style={{
            justifyContent: 'center',
          }}
        >
          <XStack alignItems="center" space>
            <IconEdit color="white" />
            <H4 color="white">
              Mes déclarations
            </H4>
          </XStack>
        </Pressable>
        <Pressable
          onPress={() => navigation.navigate('fdt')}
          style={{
            justifyContent: 'center',
          }}
        >
          <XStack alignItems="center" space>
            <IconLaptop color="white" />
            <H4 color="white">
              Déclaration de formation
            </H4>
          </XStack>
        </Pressable>
      </YStack>
    </SafeAreaView>
  );
}
