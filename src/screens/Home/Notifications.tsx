import { useCallback, useEffect, useState } from 'react';
import { View } from 'react-native';
import { FlashList } from '@shopify/flash-list';
import { useToastController } from '@tamagui/toast';
import Header from '@/components/Header';
import { useAppDispatch, useAppSelector } from '@/redux';
import NotificationCard from '@/components/NotificationCard';
import { getAllNotifications, markNotificationAsRead } from '@/redux/reducers/notifications/notifications.thunks';

export default function Notifications() {
  const dispatch = useAppDispatch();
  const toast = useToastController();
  const [loading, setLoading] = useState(false);
  const notifications = useAppSelector((state) => state.notifications.list);

  const fetchNotifications = useCallback(async () => {
    try {
      setLoading(true);
      await dispatch(getAllNotifications()).unwrap();
    } catch (error: any) {
      toast.show(
        'Une erreur est survenue',
        {
          native: true,
          duration: 3000,
          message: error.message,
          burntOptions: {
            preset: 'error',
            haptic: 'error',
          },
        },
      );
    } finally {
      setLoading(false);
    }
  }, [dispatch]);

  const markAsRead = useCallback((id: number) => async () => {
    try {
      setLoading(true);
      await dispatch(markNotificationAsRead(id)).unwrap();
    } catch (error: any) {
      toast.show(
        'Une erreur est survenue',
        {
          native: true,
          duration: 3000,
          message: error.message,
          burntOptions: {
            preset: 'error',
            haptic: 'error',
          },
        },
      );
    } finally {
      setLoading(false);
    }
  }, [dispatch]);

  useEffect(() => {
    fetchNotifications();
  }, [fetchNotifications]);

  return (
    <View style={{ flex: 1 }}>
      <Header title="Notifications" />
      <FlashList<UserNotification>
        onRefresh={fetchNotifications}
        refreshing={loading}
        data={notifications}
        renderItem={({ item }) => (
          <NotificationCard
            notification={item}
            markAsRead={markAsRead(item.id)}
          />
        )}
        keyExtractor={(item) => item.id.toString()}
        contentContainerStyle={{ padding: 16 }}
        estimatedItemSize={50}
        getItemType={(item: UserNotification) => item.type}
      />
    </View>
  );
}
