import { useState } from 'react';
import { View } from 'react-native';
import Header from '@/components/Header';

export default function Modules() {
  const [loading, setLoading] = useState(false);
  return (
    <View style={{ flex: 1 }}>
      <Header title="Modules" />

    </View>
  );
}
