interface UserNotification {
  id: number;
  message: string;
  start: Date;
  end: null;
  can_validate: boolean;
  validated: boolean;
  type: UserNotificationType;
  metas: UserNotificationMetas;
}

interface UserNotificationMetas {
  type?: UserNotificationMetasType;
  session_id?: number;
  activity_id?: number;
  activity_type?: UserNotificationMetaActivityType;
  promo?: string;
  event_type?: string;
  event_date_id?: number;
  activity?: string;
}

type UserNotificationMetaActivityType = 'project' | 'quest' | 'cours' | 'resource';

type UserNotificationMetasType = 'module_show' | 'activity_show' | 'promo_wall_show' | 'event_subscription';

type UserNotificationType = 'notice' | 'error' | 'warning';
