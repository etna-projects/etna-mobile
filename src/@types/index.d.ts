interface GetIdentityResponse {
  id: number;
  login: string;
  email: string;
  logas: boolean;
  groups: string[];
  login_date: Date;
  firstconnexion: boolean;
}

interface GetUserByIDResponse {
  id: number;
  login: string;
  firstname: string;
  lastname: string;
  email: string;
  close: boolean;
  roles: string[];
  created_at: Date;
  updated_at: Date;
  deleted_at: null;
}

interface UserPromotion {
  id: number;
  target_name: string;
  term_name: string;
  learning_start: Date;
  learning_end: Date;
  learning_duration: number;
  promo: string;
  spe: string;
  wall_name: string;
}

interface UserBrief {
  id: number;
  login: string;
  firstname: string;
  lastname: string;
  phone: string | null;
  promo: string;
  email: string;
  close: string | null;
  deleted_at: string | null;
}
