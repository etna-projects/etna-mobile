interface UserModule {
  id: number;
  name: string;
  published: 0 | 1;
  mandatory: string;
  uv_id: number;
  uv_name: string;
  version: number;
  duration: number;
  long_name: string;
  description: string;
  equivalence: null;
  date_start: Date;
  date_end: Date;
  validation: null | string;
}
