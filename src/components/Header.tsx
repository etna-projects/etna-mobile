import { NavigationProp } from '@react-navigation/native';
import React from 'react';
import {
  Animated, Pressable, StatusBar, StyleProp, View, ViewStyle,
} from 'react-native';
import {
  XStack, YStack, Text, H5,
} from 'tamagui';
import { ArrowLeft } from '@tamagui/lucide-icons';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useAppSelector } from '@/redux';
import { HEADER_HEIGHT, HEADER_HEIGHT_EXTENDED } from '@/utils/constants';
import { ETNA_GREEN, ETNA_RED } from '@/utils/colors';

interface HeaderProps {
  title: string;
  style?: StyleProp<ViewStyle> | Animated.WithAnimatedValue<StyleProp<ViewStyle>>;
  back?: { title: string } | undefined;
  navigation?: NavigationProp<any>;
  rightElement?: React.ReactNode;
}

export default function Header({
  title, style = {}, back, navigation, rightElement,
}: HeaderProps) {
  const isInternetReachable = useAppSelector((state) => state.app.isInternetReachable);
  const backgroundColor = isInternetReachable ? ETNA_GREEN : ETNA_RED;
  const textColor = 'white';
  return (
    <SafeAreaView
      edges={['top']}
      style={[{
        backgroundColor,
        height: isInternetReachable ? HEADER_HEIGHT : HEADER_HEIGHT_EXTENDED,
      }, style]}
    >
      <XStack
        justifyContent={back && back.title ? 'space-between' : 'center'}
        alignItems="center"
      >
        <StatusBar barStyle="light-content" />
        <Pressable
          style={{ flex: 1 }}
          onPress={() => back && back.title && navigation?.goBack()}
        >
          {back && back.title && (
          <Pressable
            onPress={() => navigation?.goBack()}
          >
            <ArrowLeft color="white" />
          </Pressable>
          )}
        </Pressable>
        <View style={{
          flex: 4,
          flexDirection: 'column',
        }}
        >
          <H5 textAlign="center" color={textColor}>
            {title}
          </H5>
          {!isInternetReachable && (
            <Text textAlign="center" color={textColor}>
              Connexion internet instable
            </Text>
          )}
        </View>
        <View style={{ flex: 1 }}>
          {rightElement}
        </View>
      </XStack>
    </SafeAreaView>
  );
}
