import React from 'react';
import Constants from 'expo-constants';
import * as Updates from 'expo-updates';
import {
  YStack, Image, Text, Spinner,
} from 'tamagui';
import { View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import EtnaLogo from '@/assets/images/logo/etna-logo-1-blanc.png';
import { ETNA_GREEN } from '@/utils/colors';

interface LoaderProps {
  withLogo?: boolean;
}

export default function Loader({ withLogo }:LoaderProps) {
  return (
    <SafeAreaView style={{ backgroundColor: ETNA_GREEN, flex: 1 }}>
      <YStack
        flex={1}
        space
        alignItems="center"
        justifyContent="center"
      >
        <View style={{ flex: 1 }} />
        {withLogo && (
        <View style={{
          justifyContent: 'center',
          alignItems: 'center',
        }}
        >
          <Image
            source={EtnaLogo}
            resizeMode="contain"
            width={200}
            height={200}
          />
        </View>
        )}
        <View>
          <Spinner size="large" color="white" />
        </View>
        {withLogo && (
        <>
          <View style={{ flex: 1 }} />
          <Text textAlign="center" color="white">
            {Constants.expoConfig?.runtimeVersion || Updates.manifest?.runtimeVersion}
          </Text>
          <Text textAlign="center" color="white" fontSize="$1" marginBottom="$4">
            {Updates.updateId || ''}
          </Text>
        </>
        )}
      </YStack>
    </SafeAreaView>
  );
}
