import {
  Card, Paragraph, XStack, Text,
} from 'tamagui';
import dayjs from 'dayjs';
import { ETNA_BLUE, ETNA_BLUE_BG } from '@/utils/colors';

interface NotificationCardProps {
  notification: UserNotification;
  markAsRead: () => Promise<void>;
}

const getBackgroundColor = (notification: UserNotification) => {
  if (!notification.validated) {
    return ETNA_BLUE_BG;
  }
  switch (notification.type) {
    case 'error':
      return '#f2dede';
    case 'notice':
      return '#dff0d8';
    case 'warning':
      return 'rgba(255,193,0,0.67)';
    default:
      return '#fff';
  }
};
export default function NotificationCard({
  notification,
  markAsRead,
}: NotificationCardProps) {
  return (
    <Card
      size="$1"
      bordered
      padded
      marginBottom="$2"
      onPress={notification.can_validate && !notification.validated ? markAsRead : undefined}
      backgroundColor={getBackgroundColor(notification)}
      borderColor={!notification.validated ? ETNA_BLUE : undefined}
    >
      <Card.Header padded>
        <Paragraph>
          {notification.message
            .replace(/(<([^>]+)>)/ig, '')
            .trim()
            .replace(/ +(?= )/g, '')}
        </Paragraph>
      </Card.Header>
      <Card.Footer padded>
        <XStack flex={1} />
        <Text fontWeight="100">{dayjs(notification.start).format('LLL')}</Text>
      </Card.Footer>
    </Card>
  );
}
