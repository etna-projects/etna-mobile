import BaseApi from './base';

class AchievementsApi extends BaseApi {
  constructor() {
    super();
    this.instance.defaults.baseURL = 'https://achievements.etna-alternance.net';
  }
}

const achievementsApi = new AchievementsApi();

export default achievementsApi;
