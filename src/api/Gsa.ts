import BaseApi from './base';

class GsaApi extends BaseApi {
  constructor() {
    super();
    this.instance.defaults.baseURL = 'https://gsa-api.etna-alternance.net';
  }
}

const gsaApi = new GsaApi();

export default gsaApi;
