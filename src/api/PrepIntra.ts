import BaseApi from './base';

class PrepIntraApi extends BaseApi {
  constructor() {
    super();
    this.instance.defaults.baseURL = 'https://prepintra-api.etna-alternance.net/';
  }

  /**
   * Recupère les promotions de l'utilisateur depuis son inscription à ETNA
   */
  public async getPromotion() {
    return this.instance.get<UserPromotion[]>('/promo');
  }

  /**
   * Recupère les notifications non lues de l'utilisateur depuis son inscription à ETNA
   * @param login
   */
  public async getUserUnreadNotifications(login: string) {
    return this.instance.get<UserNotification[]>(`/students/${login}/informations`);
  }
}

const prepIntraApi = new PrepIntraApi();

export default prepIntraApi;
