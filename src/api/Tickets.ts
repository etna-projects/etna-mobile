import BaseApi from './base';

class TicketsApi extends BaseApi {
  constructor() {
    super();
    this.instance.defaults.baseURL = 'https://tickets.etna-alternance.net';
  }
}

const ticketsApi = new TicketsApi();

export default ticketsApi;
