import BaseApi from './base';

class IntraApi extends BaseApi {
  constructor() {
    super();
    this.instance.defaults.baseURL = 'https://intra-api.etna-alternance.net';
  }

  public async getUserByLogin(login: string) {
    return this.instance.get<UserBrief>(`/users/${login}`);
  }

  /**
   * Recuperer toutes les notifications non lues de l'utilisateur
   * @param login
   */
  public async getArchivedNotifications(login: string) {
    return this.instance.get<UserNotification[]>(`/students/${login}/informations/archived`);
  }

  /**
   * Marquer une notification comme lue
   * @param login
   * @param notificationId
   */
  public async markNotificationAsRead(login: string, notificationId: number) {
    return this.instance.put<UserNotification>(`/students/${login}/informations/${notificationId}/validate`);
  }
}

const intraApi = new IntraApi();

export default intraApi;
