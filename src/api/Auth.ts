import BaseApi from './base';

class AuthApi extends BaseApi {
  constructor() {
    super();
    this.instance.defaults.baseURL = 'https://auth.etna-alternance.net/';
  }

  /**
   * Se connecter à son compte intra
   * @param login Votre login intra
   * @param password Votre mot de passe intra
   */
  public async login(login: string, password: string) {
    return this.instance.post('/login', {
      login,
      password,
    });
  }

  /**
   * Se déconnecter de son compte intra
   */
  public logout() {
    this.instance.defaults.headers.common.authenticator = '';
  }

  /**
   * Récupérer les informations de l'utilisateur connecté
   */
  public async identity() {
    return this.instance.get<GetIdentityResponse>('/identity');
  }

  /**
   * Récupérer les informations d'un utilisateur
   * @param id L'identifiant de l'utilisateur
   */
  public async userById(id: number) {
    return this.instance.get<GetUserByIDResponse>(`/api/users/${id}`);
  }

  /**
   * Récupérer l'url de la photo de profil d'un utilisateur
   * @param login
   */
  // eslint-disable-next-line class-methods-use-this
  public getProfilePictureUrl(login: string) {
    return `https://auth.etna-alternance.net/api/users/${login}/photo`;
  }
}

const authApi = new AuthApi();

export default authApi;
