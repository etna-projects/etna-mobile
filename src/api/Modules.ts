import BaseApi from './base';

class ModulesApi extends BaseApi {
  constructor() {
    super();
    this.instance.defaults.baseURL = 'https://modules-api.etna-alternance.net';
  }

  public async searchModules(
    login: string,
    role?: string,
    id_promo?: number,
  ) {
    return this.instance.get<UserModule[]>(`/students/${login}/search`, {
      params: {
        role,
        id_promo,
      },
    });
  }
}

const modulesApi = new ModulesApi();

export default modulesApi;
