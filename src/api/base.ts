import axios, { AxiosInstance } from 'axios';

export default class BaseApi {
  protected instance: AxiosInstance;

  constructor() {
    this.instance = axios.create({
      withCredentials: true,
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    });
  }
}
