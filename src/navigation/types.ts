import type { NavigatorScreenParams } from '@react-navigation/native';
import type { StackScreenProps } from '@react-navigation/stack';

export type HomeTabParamsList = {
  'home/menu': undefined;
  'home/modules': undefined;
  'home/notifications': undefined;
  'home/planning': undefined;
};

export type MainNavigatorParamList = {
  home: NavigatorScreenParams<HomeTabParamsList> | undefined;
  login: undefined;
  profile: undefined;
  tickets: undefined;
  grades: undefined;
  declarations: undefined;
  fdt: undefined;
};

export type MainNavigatorScreenProps<T extends keyof MainNavigatorParamList> =
StackScreenProps<MainNavigatorParamList, T>;

declare global {
  namespace ReactNavigation {
    interface RootParamList extends MainNavigatorParamList { }
  }
}
