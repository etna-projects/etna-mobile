// eslint-disable-next-line import/no-extraneous-dependencies
import { getHeaderTitle } from '@react-navigation/elements';
import { createStackNavigator } from '@react-navigation/stack';
import { useCallback, useEffect, useState } from 'react';
import Login from '@/screens/Login';
import HomeNavigator from '@/navigation/HomeNavigator';
import { useAppDispatch } from '@/redux';
import { getNetworkStatus, handleStartupTasks } from '@/redux/reducers/app/app.thunks';
import Loader from '@/components/Loader';
import Tickets from '@/screens/Tickets';
import Profile from '@/screens/Profile';
import Header from '@/components/Header';
import Grades from '@/screens/Grades';
import MyELearning from '@/screens/MyELearning';
import DeclareELearning from '@/screens/DeclareELearning';

const Stack = createStackNavigator();
export default function MainNavigator() {
  const [initialRouteName, setInitialRouteName] = useState<'login' | 'home'>('login');
  const [loading, setLoading] = useState(true);
  const dispatch = useAppDispatch();

  const populateApp = useCallback(async () => {
    try {
      setLoading(true);
      const { authenticated } = await dispatch(handleStartupTasks()).unwrap();
      setInitialRouteName(authenticated ? 'home' : 'login');
    } catch (e) {
      setInitialRouteName('login');
    } finally {
      setLoading(false);
    }
  }, [dispatch]);

  useEffect(() => {
    populateApp();
    const interval = setInterval(() => {
      dispatch(getNetworkStatus());
    }, 2500);
    return () => clearInterval(interval);
  }, [populateApp]);

  if (loading) return <Loader withLogo />;

  return (
    <Stack.Navigator
      initialRouteName={initialRouteName}
      screenOptions={{
        header: ({
          navigation, route, options, back,
        }) => {
          const title = getHeaderTitle(options, route.name);
          return (
            <Header
              title={title}
              back={back}
              navigation={navigation}
              style={options.headerStyle}
            />
          );
        },
      }}
    >
      <Stack.Screen
        name="login"
        component={Login}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="home"
        component={HomeNavigator}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="profile"
        component={Profile}
        options={{
          title: 'Profil',
        }}
      />
      <Stack.Screen
        name="tickets"
        component={Tickets}
        options={{
          title: 'Tickets',
        }}
      />
      <Stack.Screen
        name="grades"
        component={Grades}
        options={{
          title: 'Notes',
        }}
      />
      <Stack.Screen
        name="declarations"
        component={MyELearning}
        options={{
          title: 'Mes déclarations',
        }}
      />
      <Stack.Screen
        name="fdt"
        component={DeclareELearning}
        options={{
          title: 'Déclaration de formation',
        }}
      />
    </Stack.Navigator>
  );
}
