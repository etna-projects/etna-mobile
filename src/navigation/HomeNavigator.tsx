import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { Dimensions } from 'react-native';
import {
  CalendarDays as IconCalendarDays,
  Bell as IconBell,
  Menu as IconMenu,
  Book as IconBook,
} from '@tamagui/lucide-icons';
import Notifications from '@/screens/Home/Notifications';
import Planning from '@/screens/Home/Planning';
import Modules from '@/screens/Home/Modules';
import Menu from '@/screens/Home/Menu';
import { ETNA_GRAY, ETNA_GREEN } from '@/utils/colors';

const Tab = createMaterialTopTabNavigator();

export default function HomeNavigator() {
  return (
    <Tab.Navigator
      tabBarPosition="bottom"
      initialRouteName="home/planning"
      initialLayout={{
        width: Dimensions.get('window').width,
      }}
      screenOptions={{
        tabBarStyle: {
          backgroundColor: ETNA_GREEN,
          height: 80,
          paddingBottom: 10,
        },
        tabBarActiveTintColor: '#fff',
        tabBarIndicatorStyle: {
          backgroundColor: ETNA_GRAY,
          position: 'absolute',
          height: 5,
          top: 0,
        },
        tabBarItemStyle: {
          paddingHorizontal: 0,
        },
        tabBarLabelStyle: {
          fontSize: 10,

        },
      }}
    >
      <Tab.Screen
        name="home/menu"
        component={Menu}
        options={{
          title: 'Menu',
          // eslint-disable-next-line react/no-unstable-nested-components
          tabBarIcon: ({ color }) => (<IconMenu color={color} size="$2" />),
        }}
      />
      <Tab.Screen
        name="home/modules"
        component={Modules}
        options={{
          title: 'Modules',
          // eslint-disable-next-line react/no-unstable-nested-components
          tabBarIcon: ({ color }) => (<IconBook color={color} size="$2" />),
        }}
      />
      <Tab.Screen
        name="home/planning"
        component={Planning}
        options={{
          title: 'Planning',
          // eslint-disable-next-line react/no-unstable-nested-components
          tabBarIcon: ({ color }) => (<IconCalendarDays color={color} size="$2" />),
        }}
      />
      <Tab.Screen
        name="home/notifications"
        component={Notifications}
        options={{
          title: 'Notifications',
          // eslint-disable-next-line react/no-unstable-nested-components
          tabBarIcon: ({ color }) => (<IconBell color={color} size="$2" />),
        }}
      />
    </Tab.Navigator>
  );
}
